# BananaPi-Android-4.2.2-Liab
# BananaPi Android 4.2.2 sources (liab version)

=======

To get started:

apt-get install git #Install git if required

#Get the Source code

git clone https://github.com/ChrisP-Android/BananaPi-Android-4.2.2-Liab android 

#Get svox PicoTTs from google to avoid DMCA notification on this repo!  
  
cd android/android/external

git clone https://android.googlesource.com/platform/external/svox

folder android/scripts : Various scripts to build and configure (to be detailed later)

folder android/user : Various scripts and file to personnalize build without changing the code itself (to be detailed later)
